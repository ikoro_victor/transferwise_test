package algos.apples;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Created by Victor Ikoro on 6/12/2017.
 */
public class MaxApplesWithTokensExhaustive {

    private static final int MAX_ROW_COLUMN = 1024;

    private int TOKENS;
    private int[][] currentOrchard;
    private int rowCount = 0, columnCount = 0;
    private   String POINT_DELIMITER = "|";


    private List<OrderedPath> paths = new ArrayList<>();
    private OrderedPath  maxPath;

    public MaxApplesWithTokensExhaustive(int tokens)
    {
        if( tokens < 0) throw  new IllegalArgumentException("Tokens must not be negative.");
        this.TOKENS  = tokens;
    }

    public  MaxApplesWithTokensExhaustive()
    {
        this.TOKENS = 2;
    }

    private void addPath(String path){
        OrderedPath orderedPath = new OrderedPath();
        orderedPath.addPoints(path);
        paths.add(orderedPath);
    }

    public int collectApplesWithTokens(int[][] orchard){
        rowCount = orchard.length;
        columnCount =  orchard.length > 0 ? orchard[0].length : 0;
        if(rowCount * columnCount >  MAX_ROW_COLUMN)
        {
            throw new IllegalArgumentException("Matrix cell count must not be more than 400");
        }
        currentOrchard = orchard;
        paths.clear();
        maxPath= null;

        allPathsExhaustive(rowCount - 1, 0, "");
        int maxApples = 0;
        for(OrderedPath orderedPath : paths){
            if(orderedPath.getSize() > 2)
            {
                //Using the doubling tokens and adding with the current size
                int doubledEdges = 0;
                for (int i = 0; i < TOKENS; i++) {
                    doubledEdges += orderedPath.pop() * 2;
                }
                int doubledTop2Sum = doubledEdges + orderedPath.getSUM(); ;
                if(maxApples < doubledTop2Sum){
                    maxApples = doubledTop2Sum;
                    maxPath = orderedPath;
                }
            }
        }
        return maxApples;
    }

    private void allPathsExhaustive(int row, int column, String path ) {
        if (row == 0) {
            for (int i = column; i < columnCount; i++) {
                path += POINT_DELIMITER + (currentOrchard[row][i]);
            }
            addPath(path);
            return;
        }
        if (column == columnCount - 1) {
            for (int i = row; i >= 0; i--) {
                path += POINT_DELIMITER + (currentOrchard[i][column]);
            }
            addPath(path);
            return;
        }
        path += POINT_DELIMITER + (currentOrchard[row][column]);
        allPathsExhaustive(row - 1, column, path);
        allPathsExhaustive(row, column + 1, path);

    }

    private class OrderedPath
    {
        private PriorityQueue<Integer> path;

        //Original unordered path, never removed;
        private List<Integer> originalPath;
        private int N = 0;
        private int SUM = 0;



        public  OrderedPath(){
            path = new PriorityQueue<>(new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    //Max when popped
                    return o2.compareTo(o1);
                }
            });
            originalPath = new ArrayList<>();
        }

        public void add(int edge)
        {
            path.add(edge);
            originalPath.add(edge);
            N++;
            SUM += edge;
        }
        public void addPoints(String points){
            for (String val: points.split("\\" + POINT_DELIMITER)) {
                if(!"".equals(val)){
                    add(Integer.parseInt(val));
                }
            }
        }
        public int pop(){
            int val =  path.poll();
            SUM -= val;
            N--;
            return val;
        }

        public int getSUM(){ return SUM;}
        public int getSize(){return  N;}

        public List<Integer> getOriginalPath() {
            return originalPath;
        }
    }


}
