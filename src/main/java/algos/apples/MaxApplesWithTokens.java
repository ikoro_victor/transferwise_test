package algos.apples;

import static java.lang.Math.max;

/**
 * Created by Victor.Ikoro on 6/13/2017.
 */
public class MaxApplesWithTokens {

    private int rowCount = 0, columnCount = 0;
    private int[][] currentOrchard;
    private int[][][] memory;

    private int TOKENS ;

    public MaxApplesWithTokens()
    {
        this.TOKENS = 2;
    }


    public int collectApplesWithTokens(int [][] orchard){
        rowCount = orchard.length;
        columnCount =  orchard.length > 0 ? orchard[0].length : 0;
        currentOrchard = orchard;

        resetMemory(rowCount, columnCount);
        return f(rowCount - 1, 0, TOKENS);
    }

    private int f(int i, int j, int k) {
        if(i < 0 || j == columnCount)
            return 0;

        if (memory[i][j][k] != -1)
            return memory[i][j][k];

        int res = 0 ;
        if(k == 2)
        {
            res = max(
                    max(f(i-1, j, 2), f(i, j+1, 2))+ currentOrchard[i][j],
                    max(f(i-1, j, 1), f(i, j+1, 1)) + (2*currentOrchard[i][j]) );
        }
        if(k == 1){
            res = max(
                    max(f(i-1, j, 1), f(i, j+1, 1))+ currentOrchard[i][j],
                    max(f(i-1, j, 0), f(i, j+1, 0)) + (2*currentOrchard[i][j]) );
        }
        if(k == 0){
            res = max(f(i-1, j, 0), f(i, j+1, 0))+ currentOrchard[i][j];
        }
        return memory[i][j][k] = res;
    }

    private void resetMemory(int maxRow, int maxColumn){
        memory = new int[maxRow][maxColumn][TOKENS + 1];
        for (int i = 0; i < maxRow; i++) {
            for(int j = 0 ;  j < maxColumn; j++)
                for (int k =0; k < TOKENS + 1; k++ )
                    memory[i][j][k] =  -1;
        }
    }
}
