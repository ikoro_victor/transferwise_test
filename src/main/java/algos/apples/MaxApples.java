package algos.apples;

import java.util.Arrays;

/**
 * Created by Victor Ikoro on 6/11/2017.
 */
public class MaxApples {

    private int rowCount = 0, columnCount = 0;
    private int[][] currentOrchard, memory;


    public int collectApples(int [][] orchard){
        rowCount = orchard.length;
        columnCount =  orchard.length > 0 ? orchard[0].length : 0;
        currentOrchard = orchard;

        resetMemory(rowCount, columnCount);
        return f(rowCount - 1, 0);
    }

    private int f(int i, int j) {
        if(i < 0 || j == columnCount)
            return 0;

        if (memory[i][j] != -1)
            return memory[i][j];

        int result = Math.max(f(i-1, j), f(i, j+1));
        return memory[i][j] = result + currentOrchard[i][j];
    }

    private void resetMemory(int maxRow, int maxColumn){
        memory = new int[maxRow][maxColumn];
        for (int i = 0; i < maxRow; i++) {
            Arrays.fill(memory[i], -1);
        }
    }
}
