import algos.apples.MaxApplesWithTokens;
import org.junit.Test;
import util.MaxAppleUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Victor Ikoro on 6/12/2017.
 */

public class MaxApplesWithTokenTest {

    MaxApplesWithTokens maxApples = new MaxApplesWithTokens();

    @Test
    public void testMatrix5x6()
    {
        String matrixString5x6 = "5 6 6 12 24 0 0 0 1 4 2 27 5 6 0 0 0 0 0 0 1 15 1 20 4 16 20 4 7 11 0 0";
        int[][] matrix = MaxAppleUtils.buildMatrix(matrixString5x6);
        int maxNumberOfApples = maxApples.collectApplesWithTokens(matrix);
        assertEquals("Wrong apples count , expected 147 =>  20->4->7->11->20->0->27->5->6->0", 147, maxNumberOfApples);

    }

    @Test
    public void testMatrix4x6() {
        String matrixString4x6 = "4 6 1 2 3 4 5 6 7 8 9 10 11 12 13 14 16 0 0 0 0 21 22 23 0 0";
        int[][] matrix = MaxAppleUtils.buildMatrix(matrixString4x6);
        int maxNumberOfApples = maxApples.collectApplesWithTokens(matrix);
        assertEquals("Wrong max apples count , expected 150 =>  0->21->22->16->09->10->11->12->6", 150, maxNumberOfApples);
    }


    @Test
    public void testMatrix4x10MaxPathRoute4_2()
    {
        String matrixString4x10MaxPathRoute4_2  = "4 10 0 0 0 0 0 0 0 0 0 0 4 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1";
        int[][] matrix = MaxAppleUtils.buildMatrix(matrixString4x10MaxPathRoute4_2);
        int maxNumberOfApples = maxApples.collectApplesWithTokens(matrix);
        assertEquals("Wrong max apples count , expected 12 =>  0->2->4->0->0->0->0->0->0->0->0->0->0", 12, maxNumberOfApples);

    }

    @Test
    public void testMatrix4x10MaxPathRoute4_2_1()
    {
        String matrixString4x10MaxPathRoute4_2  = "4 10 0 0 0 0 0 0 0 0 0 0 4 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1";
        int[][] matrix = MaxAppleUtils.buildMatrix(matrixString4x10MaxPathRoute4_2);
        int maxNumberOfApples = maxApples.collectApplesWithTokens(matrix);
        assertEquals("Wrong max apples count , expected 12 =>  0->4->2->0->0->0->0->0->0->0->0->0->0", 12, maxNumberOfApples);

    }

    @Test
    public void testMatrix11x2MaxPathRoute2_2_2()
    {

        String matrixString11x2MaxPathRoute2_2_2  = "11 2 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 2 0 2 0 2 0 0";
        int[][]matrix = MaxAppleUtils.buildMatrix(matrixString11x2MaxPathRoute2_2_2);
        int maxNumberOfApples = maxApples.collectApplesWithTokens(matrix);
        assertEquals("Wrong max apples count , expected 10 =>   0->0->2->2->2->0->0->0->0->0->0->0", 10, maxNumberOfApples);

    }

    @Test
    public void testMatrix5x5MaxPathRoute1_0_0_2()
    {

        String matrixString5x5MaxPathRoute1_0_0_2  = "5 5 0 1 0 0 1 2 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 0";
        int[][] matrix = MaxAppleUtils.buildMatrix(matrixString5x5MaxPathRoute1_0_0_2);
        int maxNumberOfApples = maxApples.collectApplesWithTokens(matrix);
        assertEquals("Wrong max apples count , expected 8 =>  1->0->0->2->0->1->0->0->1", 8, maxNumberOfApples);

    }

    @Test
    public void testObjectReuse()
    {
        MaxApplesWithTokens maxApples = new MaxApplesWithTokens();

        String matrixString5x5MaxPathRoute1_0_0_2  = "5 5 0 1 0 0 1 2 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 0";
        int[][] matrix = MaxAppleUtils.buildMatrix(matrixString5x5MaxPathRoute1_0_0_2);
        int maxNumberOfApples = maxApples.collectApplesWithTokens(matrix);
        assertEquals("Wrong max apples count , expected 8 =>  1->0->0->2->0->1->0->0->1", 8, maxNumberOfApples);

        String matrixString11x2MaxPathRoute2_2_2  = "11 2 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 2 0 2 0 2 0 0";
        matrix = MaxAppleUtils.buildMatrix(matrixString11x2MaxPathRoute2_2_2);
        maxNumberOfApples = maxApples.collectApplesWithTokens(matrix);
        assertEquals("Wrong max apples count , expected 10 =>   0->0->2->2->2->0->0->0->0->0->0->0", 10, maxNumberOfApples);

    }

    @Test
    public void testMatrixTestForLargeMatrix()
    {
        int[][] matrix = MaxAppleUtils.buildRandomMatrix(200, 200);
        int maxNumberOfApples = maxApples.collectApplesWithTokens(matrix);
        assertTrue(maxNumberOfApples > 0);

    }




}
