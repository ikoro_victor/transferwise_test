package util;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

/**
 * Created by Victor Ikoro on 6/12/2017.
 */
public class MaxAppleUtils {

    public static int[][] buildMatrix(String s){
        ByteArrayInputStream inputStream = new ByteArrayInputStream(s.getBytes());
        Scanner scanner = new Scanner(inputStream);

        int m = scanner.nextInt();
        int n =scanner.nextInt();
        int[][] G = new int[m][n];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                G[i][j]=scanner.nextInt();
        return G;
    }

    public static int[][] buildRandomMatrix(int m, int n)
    {
        int[][] matrix = new int[m][n];
        for (int i = 0; i < m; i++) {
            for(int j = 0 ;  j < n; j++)
                    matrix[i][j] =  (int)(Math.random() * 100);
        }
        return  matrix;

    }
}
